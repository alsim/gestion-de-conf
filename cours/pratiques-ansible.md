# Pratiques ansible

* [Bonnes pratiques de developement de Rôles](#bonnes-pratiques-de-developement-de-r-les)
  * [gestion des variables](#gestion-des-variables)
    * [Variables nécessaires](#variables-n-cessaires)
    * [Variables prédéfinies](#variables-pr-d-finies)
    * [Variables déduitent](#variables-d-duitent)
  * [Les import / include](#les-import---include)
* [Organisation du code](#organisation-du-code)
  * [La plateforme playbook](#la-plateforme-playbook)
  * [Les tags](#les-tags)
* [L'inventaire dynamique](#l-inventaire-dynamique)
  * [Les config context](#les-config-context)

## Bonnes pratiques de developement de Rôles

Un fichier `README.md` sera positioné à la racine du rôle.

Il permettra d'expliquer comment utiliser ce role dans un playbook et nbotament de préciser quelle variables devras être définie et comment le role peu être paramétré.

### gestion des variables

Nous allons préfixer nos variables, celles définies et utilisées dans le rôle par le nom du rôle. Cela nous permetra de retrouver facilement ou la variable est utilisée et pour quelle raison est est mise en place.

Nous distingons 4 types de variables de rôles  et nous les documentrons par types

* les variables nécessaires, ce rôle ne peu être déployé sans que ces variable soient définient. elle ne dispose pas
* Les variables prédéfinie : elles sont définies dans le fichier defaults/main.yml elles permettent de limiter le nombre de variables à spécifier tout en permetant la surcharge de celles-ci.
* Les variables déduitent, ces variables sont en fait des template de variable qui seront évaluer pendant l'exécution. Ce le permet notament de récupèrer l'adresse ip necessaire au template de coinfiguration d'un service réseaux.

#### Variables nécessaires

Elle seront testées en début d'exécution du rôle avec  `ansible.builtin.assert`

Exemple :

```yaml
- name: is role_variable defined
  ansible.builtin.assert:
    that:
      - role_variable is defined
      - role_variable is string
  quiet: true
```

Les secrets seront aussi forcément stocké à l'extérieur du roles et feront parti des variables necessairement définie.

En revanche elle seront concervé dans un fichier de variable chiffré avec l'outil ansible-vault. ce fichier sera dons présent avec l'inventaire et le playbook (par exemple dans le dossier vars/ situé à coté du playbook)
On l'utilisera comme ceci :

```bash
$ cat role_vaulted_vars.yml
---
secret: 53cR3t
to encrypt the file
$ ansible-vault encrypt role_vaulted_vars.yml
New Vault password:
Confirm New Vault password:
Encryption successful
to view vaulted vars
$ ansible-vault view role_vaulted_vars.yml
Vault password:
---
secret: 53cR3t
```

Coté tasks on utilisera l'argument de task `no_log: true` afin de s'asurer que les logs d'exécution n'afficherons pas le secret

```yaml
    - name: Ensure API key is present in config file
      ansible.builtin.lineinfile:
        path: /etc/app/configuration.ini
        line: "API_KEY={{ api_key }}"
      no_log: True
```

A l'exéction on utilisera l'option --ask-vault-pass des commandes ansibles.

le mot de passe sera alors demander avant d'effectuer l'action

Il est aussi possible de definir dans la configuration la position d'un fichier contenant ou retournant le mot de passe : `vault_password_file = ~/.ansible-vault-pass` (dont les accès seront bien sur restreints)

#### Variables prédéfinies

Les variables prédéfinies du roles sont définie dans defaults/main.yml, elles sont en gébnérale lié au produit géré par le rôle.

Si des variable dépendent de la version d'OS sur lequel est déployé le role on définiera une tache, en générale au début des tasks tel que :

```yaml
- name: Gather OS specific variables
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}-{{ ansible_distribution_version }}.yml"
    - "{{ ansible_distribution|lower }}-{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"
    - "{{ ansible_os_family|lower }}.yml"
```

et des fichiers dans vars/

* redhat
* debian
* centos-7
* debian-10.2
* ...

Le parametre with_first_found va, comme son nom l'indique, prendre le premier fichier qu'il trouve dans la liste.
Cela nous permet de définir une règle dans le fichier le plus bas (Ex : redhat.yml) et plus tard, lors des test ou de la maintenance du rôle, definir des exceptions a cette règle sans modifier la règle de base.

#### Variables déduitent

le role vas de lui même calculer des variables, dans les facts ou par déduction à partir d'autre variable. L'objectif est de réduire le nombre de variable necessaire et ainsi facilité l'utilisation du rôle.

Exemples :

__1 - variable par hosts :__

Afin d'éviter les hosts_var d'inventaire un peu complexe à maintenir on peu utiliser un tableau de variable dont la clef et le nom du host:

Dans le playbook

```yaml
hosts: node1, node2

  vars: 
    - role_variabledehost:
      node1: valeurnode1
      node2: valeurnode2
```

dans vars/main.yml

```yaml
variabledehost: "{{ role_variabledehost[inventory_hostname] }}"
```

__2 - in vars/main.yml :__

pour recupèrer l'adresse ip et l'interface du host associé à un certain réseau; (le réseaux sera alors définie dans une variable necessaire)

dans le playbook:

```yaml
  vars:
    - role_prod_network: 192.168.56.0/24

```

dans vars/main.yml

```yaml
  role_prod_ip: "{% for iface in ansible_interfaces|sort %}{% if vars.ansible_facts[iface]['ipv4'] is defined and vars.ansible_facts[iface]['ipv4']['address'] | ipaddr(role_prod_network) %}{{ vars.ansible_facts[iface]['ipv4']['address'] }}{% endif %}{% endfor %}"
  role_prod_dev: "{% for iface in ansible_interfaces|sort %}{% if vars.ansible_facts[iface]['ipv4'] is defined and vars.ansible_facts[iface]['ipv4']['address'] | ipaddr(role_prod_network) %}{{ vars.ansible_facts[iface]['device'] }}{% endif %}{% endfor %}"
```

__3 - On peu pour un cluster définir les hosts impliqués :__

```yaml
  role_network: 192.168.56.0/24
  role_master: node1
  role_slave: node2
```

puis déduire l'ip de l'autre noeud du cluster dans vars/main.yml :

```yaml
role_other_node: "{% if inventory_hostname == role_master %}{{ role_slave }}{% elif inventory_hostname == role_slave %}{{ role_master}}{% endif %}"
role_other_ip: "{% for iface in hostvars[role_other_node].ansible_interfaces|sort %}{% if hostvars[role_other_node].ansible_facts[iface]['ipv4'] is defined and hostvars[role_other_node].ansible_facts[iface]['ipv4']['address'] | ipaddr(role_network) %}{{ hostvars[role_other_node].ansible_facts[iface]['ipv4']['address'] }}{% endif %}{% endfor %}"
```

__4 - pour plusieurs groupes de hosts est tricky :__

Dans le playbook on affecte des roles au host en fonction du fait que leur nom contiene le nom du role :

```yaml
hosts: vm_role-1_1, vm_role-1_2, vm_role-2_1, vm_role-2_2

roles:
  - role: role1
    when: inventory_hostname is match('role1')
  - role: role2
    when: inventory_hostname is match('role2')
```

dans le run chacun des host se vera déployé l'undes deux rôles.

Dans les variables du role1, on vas déduire la liste des host sur lesquels on déploie le role2 (on utilise la variable play_hosts), puis rechercher les ips de ces hosts dans certains réseaux :

```yaml
role1_role2_hosts: "{{ play_hosts | map('regex_search','.*role2.*') | select('string') | list }}"
role1_role2_priv_ips: "{% for node in role1_role2_hosts %}{% for iface in hostvars[node].ansible_interfaces|sort %}{% if hostvars[node].ansible_facts[iface]['ipv4'] is defined and hostvars[node].ansible_facts[iface]['ipv4']['address'] | ipaddr(role1_role2_prod_network) %}{{hostvars[node].ansible_facts[iface]['ipv4']['address'] }}{% endif %}{% endfor %}{% if not loop.last %}_{% endif %}{% endfor %}"
```

role1_role2_priv_ips: sea de la forme ip1_ip2

nous pouvons alors dans les task faire un loop sur un split de ces variables :

```yaml
  loop: "{{role1_role2_priv_ips.split('_') }}"
```

Pour par exemple coder des règles firewall

### Les import / include

Afin d'organiser les taches nous pourrons dédier un fichier tache a certaines taches puis dans le main.yml inclure les fichier taches un par un ou au travers de conditions.

Exemple de fichier task/main.yml:

```yaml
- import_tasks: validate_variables.yml

- name: Gather OS specific variables
  include_vars: "{{ item }}"
  with_first_found:
    - "{{ ansible_distribution|lower }}-{{ ansible_distribution_version }}.yml"
    - "{{ ansible_distribution|lower }}-{{ ansible_distribution_major_version }}.yml"
    - "{{ ansible_distribution|lower }}.yml"
    - "{{ ansible_os_family|lower }}.yml"

- include_tasks: "install_packages_{{ ansible_pkg_mgr }}.yml"

- include_tasks: configure.yml
```

import vs include : les import sont préprocessé et sont donc static durant l'exécution du playbook, les include seront traité pendant l'exécution. L'import task échoura pendant le preprocessing s'il manque des variables.

Aussi afin de développer un role fonctionnel pour plusieurs distributions plus simplement, on pourra definir des actions spécifique a certaine versions de distributions.

```yaml
- name: include OS specific tasks
  include_tasks: "{{ item }}"
  with_first_found:
    - "spec_{{ ansible_distribution|lower }}-{{ ansible_distribution_version }}.yml"
    - "spec_{{ ansible_distribution|lower }}-{{ ansible_distribution_major_version }}.yml"
    - "spec_{{ ansible_distribution|lower }}.yml"
    - "spec_{{ ansible_os_family|lower }}.yml"
    - "spec_default.yml"
```

### l'environement de test

A la racine j'ai pour habitude de placer un Vagrantfile et un playbook déployant un environnement de test du role puis le role.

## Organisation du code

Bien sur le code sera dans des dépots git. Les rôles disposent de leur propre versionning, et un dépot "ansible" représentera l'infrastructure ou la partie d'infrastruture que nous gérons avec ansible.

le dépot ansible contiendra l'inventaire les variables et les playbooks.

La gestion des changements consitera en le re-déploiement d'une version version de notre dépot (dans l'idéal)

### La plateforme playbook

Un playbook pourrais représenter une platforme de notre infrastructure, il liste les hosts de l'infrastructure, les variables **necessaire** aux roles a déployer puis les roles.

Exemple

```yaml
- name: platforme plf
  hosts: vm_role-1_1, vm_role-1_2, vm_role-2_1, vm_role-2_2

  vars: 
    - role1_serverid:
      vm_role-1_1: 21
      vm_role-1_2: 22
    - role1_prod_network: 192.168.56.0/24

  roles:
    - role: role1
      when: inventory_hostname is match('role1')
    - role: role2
      when: inventory_hostname is match('role2')
```

nous disposons ainsi d'une vision complète de la platforme en un seul fichier, toute la complexité du code est déplacé dans les roles.

### Les tags

Les tags sont des argument de tache ils permetent de regroupper plusieurs taches derrière un ou plusieurs nom : Un tag. Il est représenté par une chaine de caratère.

les tags aportent la possibiliter de ne selectionner que certaine taches du role à l'exécution. 

on associe un tag ou plusieurs dans la commande ansssible-playbook avec l'option --tags ; alors seul les taches associée à ces tags seront exécuté ansi que les taches taggé avec le tag always. Aussi le tag never associé à un autre tag sur une tache bloc l'exécution de cette tache sauf si l'autre tag est appeler explicitement. 


```yaml
- name: is role_variable defined
  ansible.builtin.debug:
    msg: " role_variable: {{ role_variable }}
  tags:
    - debuging
    - never
```

Ici toute les taches taggés "debugging" serront exécuté que lorsque que ansible-playbook sera lancer avec l'option --tags debugging 

## L'inventaire dynamique avec netbox

il existe des plugins ansible permettant d'utiliser un inventaire dynamique, l'application netbox installé précédament en founie un.

l'inventaire ne sera plus un fichier host mais un fichier yaml explicitant l'appel à l'api :

dynamic-inv.yml :

```yaml
plugin: netbox.netbox.nb_inventory
api_endpoint: http://localhost:80
validate_certs: False
token: a864d19961d51efd3a866acfdd743be32a60765c
config_context: True
compose:
  ansible_host: name
group_by:
  - device_roles
query_filters:
  - role: prod_app
  - status: active
device_query_filters:
  - has_primary_ip: 'true'
```

Il faudra créer un token sur votre compte netbox pour utiliser celui-ci.

dans l'application il faudra créer :

* un site
* un manufacturer (virtualbox)
* un type de device (vm) et lui ajouter une interface
* un device_role
* les hosts de votre infra
* Les ip des hosts de votre infra que vous associerez à vos hosts en ip primaire

Vous ajouterez dans votre configuration ansible :

```ini
[inventory]
enable_plugins = netbox.netbox.nb_inventory, auto, host_list, yaml, ini, toml, script
```

Afin d'activer le plugin d'inventaire netbox.

Enfin, cet inventaire dynamique peu maintenant être appeler à l'exécution d'ansible avec l'argument `-i`.
Je préfère génerer un inventaire local avec la commande ansible inventory pour le réutiliser par la suite.

```bash
cfg-master@master:/opt/src$ ansible-inventory -i inv --list -y >> hosts_netbox.yml
cfg-master@master:/opt/src$ ansible -i hosts_netbox.yml slave1 -m ping
slave1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python"
    },
    "changed": false,
    "ping": "pong"
}
...
```
