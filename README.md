# gestion-de-conf

## Présentation du Cours

L'infrastructures progammable ("software defined infrastructure") est à la fois une brique indissociable des aproches devops et netops et moyen d'aporter de l'agilité aux infrastructures de services.  
Ce cours se penchera principalement sur les enjeux et les technologies de gestion des configurations (Vagrant / Ansible / saltstack), leur aprehention et comment les implémenter de façon pertinente dans les projets ou dans la gestion courante d'une infrastructure de service.

Prerequis :  

Savoir utiliser git, la ligne de commande bash et avoir des notions d'administration system linux.

### Travail personnel

* Ayez un compte gitlab (de préférence à votre nom)
* Clonez ce dépot git
* Créez une branche préfixé `STU-` à votre nom : (exemple STU-simon-alan pour alan simon) et dans cette branche un dossier à votre nom-prenom dans le dossier students.
* Dans ce dossier un fichier me.txt de la forme :

```ascii
student : Nom Prenom
email : votre adresse mail
evaluation du cour :
  toutes vos remarques sur le cours sont ici les bienvenues,
```

* Vous placerez dans ce dossier vos travaux
* Vous pousserez votre branche sur le dépot git du cours régulièrement

### Contributions

Si vous trouvez des fautes d'ortophraphes à coriger (je suis dyslexique donc il y en a) ou d'autre contributions dont vous être sûr de la pertinance (ajout de definition, de ressources externes, d'exemple etc...)

* Revenez sur la branche main du dépot `git checkout main ; git pull`
* Créez une nouvelle branche FIX-le-sujet-du-fix `git checkout -b FIX-le-sujet-du-fix`
* Corrigez , ajoutez , commitez `man git`
* Poussez votre branche `git push --set-upstream`
* Faites moi une merge request (cliquez sur le lien que vous retourne gitlab)
* Retourner sur votre branche de travail

D'avance merci

## 1 - [Présentation](./cours/presentation.md)

* Contexte
* Enjeux
* Conceptes et [definitions](./cours/definitions.md)
* Les niveaux de configurations :
  ![big-picture](./cours/images/big-picture.drawio.png)
* [Principe de déploiement](./cours/presentation.md#les-principes-de-déploiement)

## 2 - Les outils

### 2.1 - [Vagrant](./cours/vagrant.md)

* Présentation
  * principes
  * Installation
  * Definitions/vocabulaire
* Utilisation
* Fonctionnement
  * les boxs
  * le dossier .vagrant
  * l'accès ssh
  * le dossier /vagrant sur les vm
  * le vagrantfile
    * la configuration par défaut
    * Les instanciations de VM
    * Les provisions
* [Travaux dirigés](./cours/TD-vagrant.md)
  * Premiers pas
  * Déploiement multi machine
  * Les provisioning
  * Exemple d'utilisation sur un projet
* [Conclusion](./cours/vagrant.md#conclusion)

#### TP vagrant

Dans votre dossier personnel students/nom-prenom créez un dossier TP-vagrant puis dedans un Vagrantfile permettant de deployer 3 VMs linux avec :

* un master et 2 en slave (256MB de ram suffisent pour les slaves)
* slave1 sous centos et slave2 sous Debian
* chacun des host avec un réseaux privé et des ip fixés
* et avec les provisions suivantes :
  * la mise à jour des fichiers /etc/hosts de chacun des hosts afin que les hosts se connaissent par leur noms
  * la synchronisation d'un sous dossier `src` du dossier courant sur le master uniquement
  * La création d'un compte **cfg-master** (membre du groupe vagrant)
  * Lui même authorisé à faire du sudo sans transmettre son mot de passe
  * Le dépot d'une clef privé sur le host master dans le dossier .ssh de l'utilisateur cfg-master
  * Le dépot de la clef publiques sur tous les hosts du fichier authorized_keys de l'utilisatreur cfg-master (sans écraser la précédente)

Bien sur, la connexion ssh par clé sera effective depuis l'utilisateur cfg-master sur le master vers les slaves et vous l'aurez testé.

Vous pousserez vbotre branche personnel sur le dépot.

### 2.2 - Ansible

#### [Présentation ansible](./cours/presentation-ansible.md)

* Contexte
  * Usages
* Principe de fonctionnement
* Vocabulaire
* Installation
* Utilisation ansible
  * L'inventaire
  * Commande ad-Hoc
    * Les modules
  * Le playbook
  * Les rôles
  * Les Collections

#### TD Utilisation Ansible

Déployez avec ansible l'application netbox en version 3.0.5 sur le master

depuis le host master et le dossier /opt/src :  Installez les roles necessaire avec ansible-galaxy

```bash
[cfg-master@master src]$ ansible-galaxy install geerlingguy.postgresql davidwittman.redis lae.netbox
.../...
```

Récupèrez le playbook fournis en example dans le role lae.netbox

```bash
[cfg-master@master src]$ cp ~/.ansible/roles/lae.netbox/examples/playbook_single_host_deploy.yml ./conf_netbox.yml
```

Editez le afin d'ajouter la variable suivante aux variable existantes:

```yaml
    netbox_stable_version: 3.0.5
```

Puis déployez l'application netbox

```bash
cfg-master@master:/opt/src$ ansible-playbook -i master, conf_netbox.yml
```

> **Sous Centos7** la compilation redis version 6. échoue car la version gcc enbarque est trop ancienne on préfèrera une version 5 on edite simplement le playbook afin de spécifier la version redis et de commenter le checksum :
>
> ```yaml
>     redis_version: 5.0.13
>     #redis_checksum: sha256:dc2bdcf81c620e9f09cfd12e85d3bc631c897b2db7a55218fd8a65eaa37f86dd
> ```
>
> **Sous debian 10**, il vous faudra installer le package python-psycopg2 necéssaire au module postgresql_user

#### [code ansible](./cours/code-ansible.md)

* Preambule
  * L'idempotence
  * Le YAML
* Les *playbooks*
  * Exemple
  * Le run d'un playbook
  * Les *tâches*
    * Les conditions
    * with_items
    * Les *register*
    * Configuration de retour
    * A retenir
* Le templating
* Les *rôles*
  * Structure d'un rôle
* Les variables
  * Les variables d'inventaires
  * Les facts
  * Précedence des variables
* Conclusion

##### TP : rédigez un playbook

Permettant le déploiement d'une application simple : gitea

Depuis le dossier TP-vagrant/src, vous créer le dossier roles et dedand toute la structure pour le role gitea :

```bash
TP-vagrant$ tree -L 3
.
├── src
│   ├── ansible.cfg
│   ├── apache-base.yml
│   ├── httpd.j2
│   └── roles
│       └── gitea
└── Vagrantfile
```

En suivant la documentation [d'installation gitea](https://docs.gitea.io/en-us/install-from-binary/)

* vous créer des variable par défaut
  * gitea_version, gitea_vardir et gitea_etcdir
* vous déclarer les taches respective pour :
  * installer le pakage git
  * créer le compte utilisateur git
  * créer les dossier necessaire à gitea (en utilisant les variables gitea_vardir et gitea_etcdir)
  * télécharger et déposer l'exécutable gitea (en utilisant la variable gitea_version)
  * templater le service systemd avec le fichier [gitea.service](https://github.com/go-gitea/gitea/blob/main/contrib/systemd/gitea.service) que vous editerez afin d'utiliser les variable définie plus haut.
  * et enfin démarrer le service
* vous créer un playbook déployant ce service sur l'un des slave
* vous deployez ce role sur le slave choisi

#### [Pratiques ansible](./cours/pratiques-ansible.md)

* Bonnes pratiques de developement de Rôles
  * gestion des variables
    * Variables nécessaires
    * Variables prédéfinies
    * Variables déduitent
  * Les import / include
* Organisation du code
  * La plateforme playbook
  * Les tags
* L'inventaire dynamique

##### TP **rendu** : coder un role qui :

* Installe et configure le produit sur une debian 10 ou une centos7 a minima
* propose optionnelement (exemple sur existance d'une variables ansible)
  * initialise le produit : création d'une instance d'usage sur le produit
  * fait un test basique de bon fonctionnement du produit

Choisir parmis : mariadb avec une base, apache et php avec un vhost et un document root, nginx et uwsgi avec une application python web, proposez quelque chose d'autre

Le Rendu dans le dossier TP_ansible de votre branche sera évalué sur : 

* un Readme : complet :
  * objet : 
  * Limites : OS et versions supportés
    * sur quel versionexact le role à été testé par vous 
  * variables 
    * necessaire
    * déduite
    * écrasable
* Le Bon fonctionnement 
  * du role pour le déploiement, 
  * pour la fonctionalité ajoutée
  * du test
* La nomenclature : 
  * toutes les taches sont correctement nommés
  * toute le variable sont correctement nommés
* la "variabilisation" complète 
  * pas de code en dure dans le role, 
  * toute variable documentée (aucun code en dure)
* le role est idempotent 
* Support de plusieur OS : debian + centos

Bien sur vous devez avoir un dépot parfaitement rangé, pas de fichier qui trainent etc...
